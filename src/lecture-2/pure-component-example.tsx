import React, {PureComponent} from 'react';

type State = {
    data: {
        count: number
    }
}

export class PureComponentExample extends PureComponent<{}, State> {
    state = {
        data: {
            count: 0
        }
    };

    updateCounter = () => {
        this.setState({
            data: {
                count: this.state.data.count + 1
            }
        });
    };

    render() {
        return (
            <div>
                {this.state.data.count}
                <button onClick={this.updateCounter}>Inc</button>
            </div>
        )
    }
}
