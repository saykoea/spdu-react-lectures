import React, {Component} from 'react';

type Props = {
    name: string
}

export class ClassExample extends Component<Props> {
    render() {
        return <div>Class example. Hello, {this.props.name}</div>;
    }
}
