import React, {useState} from 'react';
import './App.css';
import {Parent} from "./lecture-2/parent-child-example";
import {ClassExample} from "./lecture-2/class-example";
import {FunctionalExample} from "./lecture-2/functional-example";
import {PropsExample} from "./lecture-2/props-example.";
import {StateExample} from "./lecture-2/state-example";
import {InputEventHandlingExample} from "./lecture-2/input-event-handling-example";
import {PureComponentExample} from "./lecture-2/pure-component-example";
import {ContextApp} from './lecture-3/context-example/app';
import {RefsExample} from './lecture-3/refs-example';

enum View {
    CLASS_EXAMPLE,
    FUNCTIONAL_COMPONENT,
    PROPS_EXAMPLE,
    STATE_EXAMPLE,
    PARENT_CHILD_EXAMPLE,
    INPUT_EVENT_HANDLING_EXAMPLE,
    PURE_COMPONENT_EXAMPLE,
    CONTEXT,
    REFS
}


const EXAMPLES = {
    [View.CLASS_EXAMPLE]: () => <ClassExample name="SPD University"/>,
    [View.FUNCTIONAL_COMPONENT]: FunctionalExample,
    [View.PROPS_EXAMPLE]: () => <PropsExample data={[]}/>,
    [View.STATE_EXAMPLE]: () => <div><StateExample caption="Click me!" type="PRIMARY"/></div>,
    [View.PARENT_CHILD_EXAMPLE]: Parent,
    [View.INPUT_EVENT_HANDLING_EXAMPLE]: InputEventHandlingExample,
    [View.PURE_COMPONENT_EXAMPLE]: PureComponentExample,
    [View.CONTEXT]: ContextApp,
    [View.REFS]: RefsExample

};

const App: React.FC = () => {
    const [current, setCurrent] = useState<View>(View.CLASS_EXAMPLE);

    const CurrentView: React.ElementType = EXAMPLES[current];

    return (
        <div className="App">
            {/*            <button onClick={() => setCurrent(View.CLASS_EXAMPLE)}>Class Example</button>
            <button onClick={() => setCurrent(View.FUNCTIONAL_COMPONENT)}>Functional Example</button>
            <button onClick={() => setCurrent(View.PROPS_EXAMPLE)}>Props Example</button>
            <button onClick={() => setCurrent(View.STATE_EXAMPLE)}>State Example</button>
            <button onClick={() => setCurrent(View.PARENT_CHILD_EXAMPLE)}>Parent-child Example</button>
            <button onClick={() => setCurrent(View.INPUT_EVENT_HANDLING_EXAMPLE)}>Input event handling Example</button>
            <button onClick={() => setCurrent(View.PURE_COMPONENT_EXAMPLE)}>Pure component Example</button>*/}
            <button onClick={() => setCurrent(View.CONTEXT)}>Context Example</button>
            <button onClick={() => setCurrent(View.REFS)}>Refs Example</button>
            <CurrentView/>
        </div>
    );
};

export default App;
