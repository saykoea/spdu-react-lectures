import React, {ChangeEvent, Component, createRef} from 'react';

type State = {
    value: string
};

export class RefsExample extends Component<{}, State> {
    state = {
        value: ''
    };

    input = createRef<HTMLInputElement>();

    onInputChange = (e: ChangeEvent<HTMLInputElement>) => {
        this.setState({value: e.target.value});
    };

    onFocus = () => {
        if (this.input.current) {
            this.input.current.focus();
        }
    };

    render() {
        return (
            <div>
                value: {this.state.value}
                <div>
                    <button onClick={this.onFocus}>Set input focus</button>
                    <div style={{height: '1300px'}}></div>
                    <input ref={this.input} value={this.state.value} onChange={this.onInputChange}/>
                </div>
            </div>
        )
    }
}
