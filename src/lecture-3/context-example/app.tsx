import React, {FC, useEffect, useState} from 'react';
import {Navigation} from './modules/navigation/navigation';
import {AppContext} from './app.context';
import {User} from './typedef';


export const ContextApp: FC<{}> = () => {
    const [user, setUser] = useState<User>();

    useEffect(() => {
            fetch('https://api.github.com/users/vlad-musienko')
                .then(result => result.json())
                .then((githubUser) => {
                    const user = {
                        name: githubUser.login,
                        avatar: githubUser.avatar_url
                    };
                    setUser(user)
                });
        },
        []
    );


    return (
        <AppContext.Provider value={{user}}>
            <div className="container">
                <Navigation/>
                <pre>
                    {JSON.stringify(user, null, 4)}
                </pre>
            </div>
        </AppContext.Provider>
    )
};
