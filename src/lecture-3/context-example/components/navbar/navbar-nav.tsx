import React, {FC, ReactNode} from 'react';

type Props = {
    children: ReactNode
};

export const Nav: FC<Props> = props => {
    return (
        <ul className="navbar-nav mr-auto">
            {props.children}
        </ul>
    )
};
