import React, {FC, ReactNode} from 'react';

type Props = {
    children: ReactNode
};

export const Item: FC<Props> = ({children}) => {
    return (
        <li className="nav-item">
            {children}
        </li>
    )
};
