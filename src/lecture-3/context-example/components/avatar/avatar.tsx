import React, {FC} from 'react';
import classNames from 'classnames';
import './avatar.css';

type Props = {
    size: number,
    src: string
    rounded?: boolean,
};

export const Avatar: FC<Props> = ({size, rounded, src}) => {
    const imgClasses = classNames('avatar__image', {'avatar__image_rounded': rounded});
    return (
        <div className="avatar">
            <img className={imgClasses} src={src} style={{width: `${size}px`}} alt="Avatar"/>
        </div>
    )
};
