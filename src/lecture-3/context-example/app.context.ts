import {createContext} from 'react';
import {User} from './typedef';

export type Context = {
    user?: User
}

export const AppContext = createContext<Context>({});
