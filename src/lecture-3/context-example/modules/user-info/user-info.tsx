import React, {FC} from 'react';
import {Avatar} from '../../components/avatar/avatar';
import {AppContext} from '../../app.context';


export const UserInfo: FC<{}> = () => {
    return (
        <AppContext.Consumer>
            {({user}) => {
                return user
                    ? (
                        <>
                            <Avatar size={16} rounded={true} src={user.avatar}/>
                            <strong>{user.name}</strong>
                        </>
                    )
                    : null
            }}
        </AppContext.Consumer>
    )
};
